package com.ewa.ribbit.utils;

/**
 * Created by Ewa on 10/17/2015.
 */
public final class ParseConstants {
    //Class names
    public static final String CLASS_MESSAGES = "Messages";

    //Field names
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FRIENDS_RELATION = "friendsRelation";
    public static final String KEY_FIRSTNAME = "firstname";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_HOMETOWN = "hometown";
    public static final String KEY_WEBSITE = "website";
    public static final String KEY_BIRTHDATE = "birthday";
    public static final String KEY_RECIPIENT_IDS = "recipientIds";
    public static final String KEY_SENDER_ID = "senderID";
    public static final String KEY_SENDER_NAME = "senderName";
    public static final String KEY_FILE = "file";
    public static final String KEY_FILE_TYPE = "fileType";
    public static final String KEY_CREATED_AT = "createdAt";
    public static final String KEY_TEXT = "text";
    public static final String KEY_USER_ID = "userId";

    //misc values
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_WRITTEN = "written";


}
