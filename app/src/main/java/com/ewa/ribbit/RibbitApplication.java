package com.ewa.ribbit;

import android.app.Application;

import com.ewa.ribbit.utils.ParseConstants;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseUser;


/**
 * Created by Ewa on 10/12/2015.
 */
public class RibbitApplication extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        // Enable Local Datastore.

        Parse.enableLocalDatastore(this);

        Parse.initialize(this, "qw4meQzel1DEMvmlDiy0wWAiZ4thRg2xpUrjkQCL", "fz1zoqTQXgOH0THA4sMyXYCYNjSvG3QzMhNGFFCQ");
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

    public static void updateParseInstallation(ParseUser user){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(ParseConstants.KEY_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }
}
