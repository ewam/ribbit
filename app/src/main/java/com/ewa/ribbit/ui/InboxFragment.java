package com.ewa.ribbit.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ewa.ribbit.adapters.MessageAdapter;
import com.ewa.ribbit.utils.ParseConstants;
import com.ewa.ribbit.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ewa on 10/16/2015.
 */
public class InboxFragment extends ListFragment {

    protected List<ParseObject> mMessages;
    protected ProgressBar mInboxProgressBar;
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inbox, container, false);
        mInboxProgressBar = (ProgressBar) rootView.findViewById(R.id.inboxProgressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout);

        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.swipeRefresh1, R.color.swipeRefresh2,
                R.color.swipeRefresh3, R.color.swipeRefresh4);
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        retrieveMessages();
    }

    private void retrieveMessages() {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(ParseConstants.CLASS_MESSAGES);
        query.whereEqualTo(ParseConstants.KEY_RECIPIENT_IDS, ParseUser.getCurrentUser().getObjectId());
        query.addDescendingOrder(ParseConstants.KEY_CREATED_AT);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> messages, ParseException e) {
                mInboxProgressBar.setVisibility(View.GONE);
                if(mSwipeRefreshLayout.isRefreshing()){
                    mSwipeRefreshLayout.setRefreshing(false);
                }

                if(e == null){
                    mMessages = messages;


                    if(getListView().getAdapter() == null) {
                        MessageAdapter adapter = new MessageAdapter(
                                getListView().getContext(), mMessages);
                        setListAdapter(adapter);
                    }
                    else{
                        //refill the adapter
                        ((MessageAdapter)getListView().getAdapter()).refill(mMessages);
                    }
                }
            }
        });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        ParseObject message = mMessages.get(position);
        String messageType = message.getString(ParseConstants.KEY_FILE_TYPE);
        if(messageType.equals(ParseConstants.TYPE_WRITTEN)){
            String messageText = message.getString(ParseConstants.KEY_TEXT);
            Intent viewTextIntent = new Intent(getActivity(), ViewTextActivity.class);
            viewTextIntent.putExtra(ParseConstants.KEY_TEXT, messageText);
            startActivity(viewTextIntent);
        }
        else {
            ParseFile file = message.getParseFile(ParseConstants.KEY_FILE);
            Uri fileUri = Uri.parse(file.getUrl());
            if (messageType.equals(ParseConstants.TYPE_IMAGE)) {
                //view image
                Intent viewImageIntent = new Intent(getActivity(), ViewImageActivity.class);
                viewImageIntent.setData(fileUri);
                startActivity(viewImageIntent);
            } else {
                //view the video
                Intent viewVideoIntent = new Intent(Intent.ACTION_VIEW, fileUri);
                viewVideoIntent.setDataAndType(fileUri, "video/*");
                startActivity(viewVideoIntent);
            }


        }
        //delete it!
        List<String> ids = message.getList(ParseConstants.KEY_RECIPIENT_IDS);
        if(ids.size() == 1){
            //last recipient - delete the whole thing!
            message.deleteInBackground();
        }
        else{
            //remove the recipient and save
            ids.remove(ParseUser.getCurrentUser().getObjectId());
            ArrayList<String> idsToRemove = new ArrayList<String>();
            idsToRemove.add(ParseUser.getCurrentUser().getObjectId());

            message.removeAll(ParseConstants.KEY_RECIPIENT_IDS, idsToRemove);
            message.saveInBackground();
        }
    }

    protected SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            retrieveMessages();
        }
    };
}
