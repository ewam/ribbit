package com.ewa.ribbit.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ewa.ribbit.utils.ParseConstants;
import com.ewa.ribbit.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class WriteMessageActivity extends AppCompatActivity {

    @Bind(R.id.writeMessageText)EditText mWriteMessageText;
    @Bind(R.id.writeMessageButton) Button mDoneButton;

    String mMessageText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMessageText = mWriteMessageText.getText().toString();
                if(mMessageText.isEmpty()){
                    Toast.makeText(WriteMessageActivity.this, R.string.write_message_warning, Toast.LENGTH_LONG).show();
                }
                else{
                    Intent intent = new Intent(WriteMessageActivity.this, RecipientsActivity.class);
                    intent.putExtra(ParseConstants.KEY_TEXT, mMessageText);
                    intent.putExtra(ParseConstants.KEY_FILE_TYPE, ParseConstants.TYPE_WRITTEN);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

}
