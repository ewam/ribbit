package com.ewa.ribbit.ui;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.ewa.ribbit.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PasswordResetActivity extends AppCompatActivity {

    @Bind(R.id.emailField)EditText mEmail;
    @Bind(R.id.resetPwordButton)
    Button mResetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        ButterKnife.bind(this);

        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();

                email = email.trim();

                if(email.isEmpty()){
                    //no email given/edittext is empty
                    AlertDialog.Builder builder = new AlertDialog.Builder(PasswordResetActivity.this);
                    builder.setMessage(R.string.reset_error_message)
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else{
                    //email was input
                    ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
                        public void done(ParseException e) {
                            if (e == null) {
                                // An email was successfully sent with reset instructions.
                                AlertDialog.Builder builder = new AlertDialog.Builder(PasswordResetActivity.this);
                                builder.setMessage(R.string.reset_success_message)
                                        .setTitle(R.string.success_title)
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } else {
                                // Something went wrong. Look at the ParseException to see what's up.
                                AlertDialog.Builder builder = new AlertDialog.Builder(PasswordResetActivity.this);
                                builder.setMessage(e.getMessage())
                                        .setTitle(R.string.error_title)
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                }
            }
        });
    }
}
