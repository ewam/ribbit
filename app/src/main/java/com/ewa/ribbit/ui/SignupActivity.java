package com.ewa.ribbit.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.ewa.ribbit.RibbitApplication;
import com.ewa.ribbit.utils.ParseConstants;
import com.ewa.ribbit.R;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {

    @Bind(R.id.usernameField)
    EditText mUsername;
    @Bind(R.id.passwordField) EditText mPassword;
    @Bind(R.id.emailField) EditText mEmail;
    @Bind(R.id.signupButton)
    Button mSignUpButton;
    @Bind(R.id.signupProgressBar)
    ProgressBar mSignupProgressBar;
    @Bind(R.id.firstNameField)EditText mFirstName;
    @Bind(R.id.lastNameField) EditText mLastName;
    @Bind(R.id.hometownField) EditText mHometown;
    @Bind(R.id.birthdayField) EditText mBirthday;
    @Bind(R.id.websiteField) EditText mWebsite;
    @Bind(R.id.cancelButton) Button mCancelButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.hide();
        }

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUsername.getText().toString();
                String password = mPassword.getText().toString();
                String email = mEmail.getText().toString();
                String firstname = mFirstName.getText().toString();
                String lastname = mLastName.getText().toString();
                String website = mWebsite.getText().toString();
                String birthday = mBirthday.getText().toString();
                String hometown = mHometown.getText().toString();

                username = username.trim();
                password = password.trim();
                email = email.trim();
                firstname = firstname.trim();
                lastname = lastname.trim();
                website = website.trim();
                birthday = birthday.trim();
                hometown = hometown.trim();

                if(username.isEmpty() || password.isEmpty() || email.isEmpty()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                    builder.setMessage(R.string.signup_error_message)
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
                else{
                    //create the new user
                    mSignupProgressBar.setVisibility(View.VISIBLE);
                    ParseUser newUser = new ParseUser();
                    newUser.setUsername(username);
                    newUser.setPassword(password);
                    newUser.setEmail(email);
                    newUser.put(ParseConstants.KEY_FIRSTNAME, firstname);
                    newUser.put(ParseConstants.KEY_LASTNAME, lastname);
                    newUser.put(ParseConstants.KEY_HOMETOWN, hometown);
                    newUser.put(ParseConstants.KEY_BIRTHDATE, birthday);
                    newUser.put(ParseConstants.KEY_WEBSITE, website);

                    newUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            mSignupProgressBar.setVisibility(View.INVISIBLE);
                            if(e == null){
                                //success!
                                RibbitApplication.updateParseInstallation(
                                        ParseUser.getCurrentUser());

                                Intent intent = new Intent(SignupActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                                builder.setMessage(e.getMessage())
                                        .setTitle(R.string.error_title)
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }
                    });
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }
}
