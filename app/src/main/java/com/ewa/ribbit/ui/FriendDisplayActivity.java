package com.ewa.ribbit.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ewa.ribbit.utils.ParseConstants;
import com.ewa.ribbit.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FriendDisplayActivity extends AppCompatActivity {

    @Bind(R.id.friendNameText)
    TextView mUsername;
    @Bind(R.id.fullNameText) TextView mFullName;
    @Bind(R.id.hometownDisplayText) TextView mHometown;
    @Bind(R.id.birthdateDisplayText) TextView mBirthdate;
    @Bind(R.id.websiteDisplayText) TextView mWebsite;
    @Bind(R.id.emailDisplayText) TextView mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_display);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String displayFriendID = intent.getStringExtra("FriendID");



        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.getInBackground(displayFriendID, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser user, ParseException e) {
                String firstname="";
                String lastname="";
                String fullname;
                String hometown;
                String website;
                String birthday;

                if (e == null){
                    mUsername.setText(user.getUsername());
                    mEmail.setText(user.getEmail());
                    if(user.get(ParseConstants.KEY_FIRSTNAME) != null){
                        firstname = (String)user.get(ParseConstants.KEY_FIRSTNAME);
                    }
                    if(user.get(ParseConstants.KEY_LASTNAME) != null){
                        lastname = (String) user.get(ParseConstants.KEY_LASTNAME);
                    }
                    if(!(firstname.isEmpty() && lastname.isEmpty())){
                        fullname = firstname + " " + lastname;
                        mFullName.setText(fullname);
                    }
                    if(user.get(ParseConstants.KEY_HOMETOWN) != null){
                        hometown = (String) user.get(ParseConstants.KEY_HOMETOWN);
                        if(!hometown.isEmpty()) {
                            mHometown.setText(hometown);
                            mHometown.setVisibility(View.VISIBLE);
                        }
                    }
                    if(user.get(ParseConstants.KEY_WEBSITE) != null){
                        website = (String) user.get(ParseConstants.KEY_WEBSITE);
                        if(!website.isEmpty()){
                            mWebsite.setText(website);
                            mWebsite.setVisibility(View.VISIBLE);
                        }

                    }
                    if(user.get(ParseConstants.KEY_BIRTHDATE) != null){
                        birthday = (String) user.get(ParseConstants.KEY_BIRTHDATE);
                        if(!birthday.isEmpty()){
                            mBirthdate.setText(birthday);
                            mBirthdate.setVisibility((View.VISIBLE));
                        }
                    }



                    }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(FriendDisplayActivity.this);
                    builder.setMessage(e.getMessage())
                            .setTitle(R.string.error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



}
